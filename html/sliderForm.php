<?php
$sliderName = $data['sliderName'];
$sliderNameData = str_replace(' ','-', $sliderName);

$categoryId = $data['sliderData']['category']['id'];
$categoryLink = $data['sliderData']['category']['link'];
$term = get_term_by( 'id', $categoryId, 'product_cat' );
$products = isset($data['sliderData']['products']) ? $data['sliderData']['products'] : [];
?>
<li class="slider-wrapper" data-slider-name="<?=$sliderNameData?>">
    <div class="row title">
        <div class="col">
            <h4 data-slider-name="<?=$sliderNameData?>" class="slider-heading"><?php _e('Slider title: ', 'gfShopTheme'); ?><?=$sliderName?></h4>
        </div>
        <div class="col">
            <button class="delete-slider btn btn-danger float-right"><?php _e('Delete slider', 'gfShopTheme'); ?></button>
            <button class="btn btn-primary float-right"><a href="/wp-admin/admin.php?page=gf-product-slider&tab=form&sliderId=<?=$data['sliderId']
                ?>" style="color:white"><?= __('Edit slider', 'gfShopTheme') ?></a></button>
        </div>
    </div>
    <div class="content">
        <div class="row">
            <div class="col-4">
                <label for="categorySelect"><?php _e('Category:', 'gfShopTheme'); ?> <?=$term->name?></label>

            </div>
            <div class="col-6">
                <label for="linkInput"><?php _e('Links to:', 'gfShopTheme'); ?> <?=$categoryLink?></label>
            </div>
        </div>
        <h4><?=__('Products', 'gfShopTheme')?></h4>
        <ul data-product-count="<?= count($data['sliderData']['products']) ?>" class="list-unstyled list-inline sortable product-list">
            <?php
            if ( isset( $data['sliderData']['products'] ) && count( $data['sliderData']['products'] ) > 0 ) {
                $i = 0;
                foreach ( $data['sliderData']['products'] as $product ) {


                    if (!isset($product['id'])) {
                        $product = wc_get_product($product);
                    } else {
                        $product = wc_get_product($product['id']);
                    }
                    if (!$product) {
                        continue;
                    }

                    $imageSrc = get_the_post_thumbnail_url($product->get_id());
                    $title = $product->get_title();
                    echo '
                <li class="list-inline-item">
                 <p class="productTitle">'.$product->get_title().'</p>
                    <div class="image-preview-wrapper">
                        <img class="image-preview" src="' . $imageSrc . '" alt="" height="130px" width="130px">
                    </div>
                </li>';
                    $i ++;
                }
            } ?>
        </ul>
    </div>
</li>