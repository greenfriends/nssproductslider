<?php
if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
}
$args = array(
    'orderby' => 'name',
    'order' => 'ASC',
    'hierarchical' => 1,
    'hide_empty' => '0'
);
$cats = get_terms('product_cat', $args);
?>
<style>
    .slider-wrapper .content {display: none}
</style>
<div class="wrap" id="productSlider">
    <?php settings_errors(); ?>
    <div class="row">
        <div class="col">
            <h1 id="categoryProductSlider">Pregled slajdera</h1>
        </div>
    </div>
    <div class="container-fluid">
        <ul id="forms-container" class="list-unstyled">
            <?php
            if (count($sliders) > 0) {
                foreach ($sliders as $slider) {
                    $empty = true;
                    $data = [
                        'sliderName' => $slider->title,
                        'sliderId' => $slider->slideId, // sliderId
                        'sliderData' => [
                            'category' => ['id' => $slider->categoryId, 'link' => $slider->url],
                            'products' => (array) unserialize($slider->items)
                        ]
                    ];
                    require 'sliderForm.php';
                }
            } ?>
        </ul>
    </div>
</div>

<template id="image-template">
    <li class="list-inline-item">
        <p class="product-title" style="width:220px"></p>
        <div class="image-preview-wrapper">
            <img class="image-preview" src="" alt="" height="150px" width="150px">
            <button class="remove-image btn btn-danger">&times</button>
        </div>
        <input class="product-id-input" type="hidden" value="">
    </li>
</template>