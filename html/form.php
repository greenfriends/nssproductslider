<?php
if (!current_user_can('manage_options')) {
    wp_die(__('You do not have sufficient permissions to access this page.'));
}
$cats = get_terms('product_cat', [
    'orderby' => 'name',
    'order' => 'ASC',
    'hierarchical' => 1,
    'hide_empty' => '0']);
$sliderId = 0;
$products = false;
$title = false;
$url = false;
$catId = false;
if (isset($_GET['sliderId']) && $_GET['sliderId'] !== '') {
    $sliderId = (int) $_GET['sliderId'];
    $slider = getSlider($sliderId)[0];
    $products = unserialize($slider->items);
    $title = $slider->title;
    $url = $slider->url;
    $catId = $slider->categoryId;
}
?>
<div class="wrap" id="productSlider">
    <?php settings_errors(); ?>
    <div class="row">
        <div class="col">
        <?php if ($sliderId): ?>
            <h1 id="categoryProductSlider">Izmeni slajder</h1>
        <?php else: ?>
            <h1 id="categoryProductSlider">Novi slajder</h1>
        <?php endif; ?>
        </div>
    </div>
    <form method="post" id="sliderForm" class="container-fluid" action="/wp-admin/admin.php?page=gf-product-slider&tab=saveSlider&sliderId=<?=$sliderId?>">
        <ul id="forms-container" class="list-unstyled">
            <li class="slider-wrapper">
                <div class="row title">
                    <div class="col-2">
                        <label for="sliderTitle">
                            <?php _e('Slider title:', 'gfShopTheme'); ?>
                        </label><br />
                        <input value="<?=$title?>" class="slider-name-input" name="sliderName" type="text" placeholder="<?php _e('Enter Slider Title', 'gfShopTheme'); ?>" />
                    </div>
                    <div class="col-3">
                        <label for="category">
                            <?php _e('Select category:', 'gfShopTheme'); ?>
                        </label>
                        <select class="categorySelect" name="category" id="category">
                            <?php foreach ($cats as $cat) :
                                $categoryLink = get_term_link($cat->term_id); ?>
                                <?php if ($cat->term_id == $catId): ?>
                                <option selected data-cat-slug="<?=$categoryLink?>" value="<?=$cat->term_id?>"><?=$cat->name?></option>
                            <?php else: ?>
                                <option data-cat-slug="<?=$categoryLink?>" value="<?=$cat->term_id?>"><?=$cat->name?></option>
                            <?php endif; ?>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-6">
                        <label for="linkInput">
                            <?php _e('Link to:', 'gfShopTheme'); ?>
                        </label>
                        <input class="linkInput" name="linkInput" value="<?=$url?>" />
                    </div>
                </div>
                <div class="content">
                    <p><b><?= __('Products', 'gfShopTheme') ?></b></p>
                    <div class="row">
                        <div class="col">
                            <input type="text" placeholder="<?= __('Enter product sku') ?>">
                            <button class="btn btn-primary addProduct"><?= __('Add product', 'gfShopTheme') ?></button>
                        </div>
                    </div>
                    <ul class="list-unstyled list-inline sortable product-list" data-product-count="<?=count($products)?>">
                    <?php if (is_array($products)) {
                        foreach ($products as $product) {

                            if (!isset($product['id'])) {
                                $product = wc_get_product($product);
                            } else {
                                $product = wc_get_product($product['id']);
                            }
                            if (!$product) {
                                continue;
                            }

                            $imageSrc = get_the_post_thumbnail_url($product->get_id());
                            $title = $product->get_title();
                            echo '
                        <li class="list-inline-item">
                            <p style="width:220px">' . $product->get_title() . '</p>
                            <div class="image-preview-wrapper">
                                <img class="image-preview" src="' . $imageSrc . '" alt="" height="150px" width="150px">
                                <button class="remove-image btn btn-danger">&times</button>
                            </div><input name="products[id][]" class="product-id-input" type="hidden" value="' . $product->get_id() . '">
                        </li>';
                        }
                    } ?>
                    </ul>
                    <button class="btn btn-primary saveSlider"><?= __('Save slider', 'gfShopTheme') ?></button>
                </div>
            </li>
        </ul>
        <?php //submit_button(); ?>
    </form>
</div>

<template id="image-template">
    <li class="list-inline-item">
        <p class="product-title" style="width:220px"></p>
        <div class="image-preview-wrapper">
            <img class="image-preview" src="" alt="" height="150px" width="150px">
            <button class="remove-image btn btn-danger">&times</button>
        </div>
        <input class="product-id-input" type="hidden" value="">
    </li>
</template>